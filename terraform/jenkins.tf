
resource "google_compute_instance_group" "jenkins" {
  name      = "jenkins-ig"
  zone      = var.zone
  instances = [google_compute_instance.jenkins-1.self_link]

  named_port {
    name = "http"
    port = "80"
  }
}

resource "google_compute_instance" "jenkins-1" {
  name         = "jenkins-1"
  machine_type = "n1-standard-1"
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = "ubuntu-1604-lts"
      size  = "20"
    }
  }

  service_account {
    email = "jenkins@mygcpproject.iam.gserviceaccount.com"
    scopes = ["cloud-platform"]
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}


resource "google_compute_health_check" "jenkins-hc" {
  name = "jenkins-hc"

  timeout_sec         = 5
  check_interval_sec  = 10
  healthy_threshold   = 1
  unhealthy_threshold = 3

  tcp_health_check {
    port = "4141"
  }
}
