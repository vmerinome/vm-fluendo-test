provider "google" {
  region      = var.region
  project     = var.project_id
  zone        = var.zone
}

terraform {
  backend "gcs" {
    bucket      = "mybucket"
    prefix      = "terraform/state/gce"
  }
}
