class profile::jenkins {
  $version =  lookup( { 'name' => 'jenkins_version', 'default_value' => 'lts' } )

  user { 'jenkins':
    ensure => present,
    uid    => '1015',
  }

  exec { 'docker-system-user-jenkins':
    command => '/usr/sbin/usermod -aG docker jenkins',
    unless  => "/bin/cat /etc/group | grep '^docker:' | grep -qw jenkins",
    require => Class['docker']
  }

  file { '/mnt/jenkins':
    ensure => directory,
    owner  => 'jenkins',
    group  => 'jenkins',
  }

  file { '/mnt/jenkins/data':
    ensure => directory,
    owner  => 'jenkins',
    group  => 'jenkins',
  }

  file { '/var/lib/jenkins':
    ensure => link,
    target => '/mnt/jenkins/data',
  }

  mount {'jenkins_lib':
    ensure   => mounted,
    name     => '/mnt/jenkins/data',
    atboot   => true,
    device   => '/dev/disk/by-id/google-jenkins-1-disk1',
    remounts => false,
    fstype   => 'ext4',
    options  => 'discard,defaults',
    require  => [File['/mnt/jenkins/data']],
    before   => Docker::Run['jenkins']
  }

  docker::run { 'jenkins':
    image            => "jenkins:${version}",
    ports            => [ '8080:8080', '50000:50000' ],
    volumes          => [
      '/var/lib/jenkins:/var/jenkins_home:rw',
    ],
    username         => '1015',
    extra_parameters => [ '--group-add 1015' ],
    net              => 'host',
    require          => [ File['/var/lib/jenkins'] ],
  }
}
